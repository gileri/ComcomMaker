###########################################################################
##                                                                       ##
## Copyrights Etienne Chové <chove@crans.org> 2009                       ##
##                                                                       ##
## This program is free software: you can redistribute it and/or modify  ##
## it under the terms of the GNU General Public License as published by  ##
## the Free Software Foundation, either version 3 of the License, or     ##
## (at your option) any later version.                                   ##
##                                                                       ##
## This program is distributed in the hope that it will be useful,       ##
## but WITHOUT ANY WARRANTY; without even the implied warranty of        ##
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         ##
## GNU General Public License for more details.                          ##
##                                                                       ##
## You should have received a copy of the GNU General Public License     ##
## along with this program.  If not, see <http://www.gnu.org/licenses/>. ##
##                                                                       ##
###########################################################################

import re, subprocess, sys, os, sets, time, bz2, xml, gzip, io
from xml.sax import make_parser, handler
from xml.sax.saxutils import XMLGenerator, quoteattr

###########################################################################

class dummylog:
    def log(self, text):
        return

###########################################################################

class OsmSaxReader(handler.ContentHandler):

    def log(self, txt):
        self._logger.log(txt)
    
    def __init__(self, filename, logger = dummylog()):
        self._filename = filename
        self._logger   = logger
        
    def _GetFile(self):
        if type(self._filename) == file:
            return self._filename
        elif self._filename.endswith(".bz2"):
            return bz2.BZ2File(self._filename)
        elif self._filename.endswith(".gz"):
            return gzip.open(self._filename)
        else:
            return open(self._filename)
        
    def CopyTo(self, output):
        self._debug_in_way      = False
        self._debug_in_relation = False
        self.log("starting nodes")
        self._output = output
        parser = make_parser()
        parser.setContentHandler(self)
        parser.parse(self._GetFile())

    def startElement(self, name, attrs):
        attrs = attrs._attrs
        if name == "changeset":
            self._tags = {}
        elif name == "node":
            attrs["id"] = int(attrs["id"])
            attrs["lat"] = float(attrs["lat"])
            attrs["lon"] = float(attrs["lon"])
            attrs["version"] = int(attrs["version"])
            self._data = attrs
            self._tags = {}
        elif name == "way":
            if not self._debug_in_way:
                self._debug_in_way = True
                self.log("starting ways")
            attrs["id"] = int(attrs["id"])
            attrs["version"] = int(attrs["version"])
            self._data = attrs
            self._tags = {}
            self._nodes = []
        elif name == "relation":
            if not self._debug_in_relation:
                self._debug_in_relation = True
                self.log("starting relations")
            attrs["id"] = int(attrs["id"])
            attrs["version"] = int(attrs["version"])
            self._data = attrs
            self._members = []
            self._tags = {}
        elif name == "nd":
            self._nodes.append(int(attrs["ref"]))
        elif name == "tag":
            self._tags[attrs["k"]] = attrs["v"]
        elif name == "member":
            attrs["ref"] = int(attrs["ref"])
            self._members.append(attrs)

    def endElement(self, name):
        if name == "node":
            self._data["tag"] = self._tags
            try:
                self._output.NodeCreate(self._data)
            except:
                print(self._data)
                raise
        elif name == "way":
            self._data["tag"] = self._tags
            self._data["nd"]  = self._nodes
            try:
                self._output.WayCreate(self._data)
            except:
                print(self._data)
                raise
        elif name == "relation":
            self._data["tag"]    = self._tags
            self._data["member"] = self._members
            try:
                self._output.RelationCreate(self._data)
            except:
                print(self._data)
                raise

###########################################################################

class OscSaxReader(handler.ContentHandler):

    def log(self, txt):
        self._logger.log(txt)

    def __init__(self, filename, logger = dummylog()):
        self._filename = filename
        self._logger   = logger
 
    def CopyTo(self, output):
        self._output = output
        parser = make_parser()
        parser.setContentHandler(self)
        if self._filename.endswith(".bz2"):
            f = bz2.BZ2File(self._filename)
        elif self._filename.endswith(".gz"):
            f = gzip.open(self._filename)
        else:
            f = open(self._filename)                
        parser.parse(f)
        
    def startElement(self, name, attrs):
        attrs = attrs._attrs
        if name == "create":
            self._action = name
        elif name == "modify":
            self._action = name
        elif name == "delete":
            self._action = name
        elif name == "node":
            attrs["id"] = int(attrs["id"])
            attrs["lat"] = float(attrs["lat"])
            attrs["lon"] = float(attrs["lon"])
            attrs["version"] = int(attrs["version"])
            self._data = attrs
            self._tags = {}
        elif name == "way":
            attrs["id"] = int(attrs["id"])
            attrs["version"] = int(attrs["version"])
            self._data = attrs
            self._tags = {}
            self._nodes = []
        elif name == "relation":
            attrs["id"] = int(attrs["id"])
            attrs["version"] = int(attrs["version"])
            self._data = attrs
            self._members = []
            self._tags = {}
        elif name == "nd":
            self._nodes.append(int(attrs["ref"]))
        elif name == "tag":
            self._tags[attrs["k"]] = attrs["v"]
        elif name == "member":
            attrs["ref"] = int(attrs["ref"])
            self._members.append(attrs)

    def endElement(self, name):
        if name == "node":
            self._data["tag"] = self._tags
            if self._action == "create":
                self._output.NodeCreate(self._data)
            elif self._action == "modify":
                self._output.NodeUpdate(self._data)
            elif self._action == "delete":
                self._output.NodeDelete(self._data)             
        elif name == "way":
            self._data["tag"] = self._tags
            self._data["nd"]  = self._nodes
            if self._action == "create":
                self._output.WayCreate(self._data)
            elif self._action == "modify":
                self._output.WayUpdate(self._data)
            elif self._action == "delete":
                self._output.WayDelete(self._data)  
        elif name == "relation":
            self._data["tag"]    = self._tags
            self._data["member"] = self._members
            if self._action == "create":
                self._output.RelationCreate(self._data)
            elif self._action == "modify":
                self._output.RelationUpdate(self._data)
            elif self._action == "delete":
                self._output.RelationDelete(self._data)  
            return

###########################################################################

def _formatData(data):
    data = dict(data)
    if "tag" in data:
        data.pop("tag")
    if "nd" in data:
        data.pop("nd")
    if "member" in data:
        data.pop("member")
    if "visible" in data:
        data["visible"] = str(data["visible"]).lower()
    if "id" in data:
        data["id"] = str(data["id"])
    if "lat" in data:
        data["lat"] = str(data["lat"])
    if "lon" in data:
        data["lon"] = str(data["lon"])
    if "changeset" in data:
        data["changeset"] = str(data["changeset"])
    if "version" in data:
        data["version"] = str(data["version"])
    if "uid" in data:
        data["uid"] = str(data["uid"])
    return data

class OsmSaxWriter(XMLGenerator):

    def __init__(self, out, enc):
        if type(out) == str:
            XMLGenerator.__init__(self, open(out, "w"), enc)
        else:
            XMLGenerator.__init__(self, out, enc)
    
    def startElement(self, name, attrs):
        self._write('<' + name)
        for name, value in attrs.items():
            self._write(' %s=%s' % (name, quoteattr(value)))
        self._write('>\n')
        
    def endElement(self, name):
        self._write('</%s>\n' % name)
    
    def Element(self, name, attrs):
        self._write('<' + name)
        for name, value in attrs.items():
            self._write(' %s=%s' % (name, quoteattr(value)))
        self._write(' />\n')

    def NodeCreate(self, data):
        if not data:
            return
        if data["tag"]:
            self.startElement("node", _formatData(data))
            for k, v in data["tag"].items():
                self.Element("tag", {"k":k, "v":v})
            self.endElement("node")
        else:
            self.Element("node", _formatData(data))
    
    def WayCreate(self, data):
        if not data:
            return
        self.startElement("way", _formatData(data))
        for k, v in data["tag"].items():
            self.Element("tag", {"k":k, "v":v})
        for n in data["nd"]:
            self.Element("nd", {"ref":str(n)})
        self.endElement("way")
    
    def RelationCreate(self, data):
        if not data:
            return
        self.startElement("relation", _formatData(data))
        for k, v in data["tag"].items():
            self.Element("tag", {"k":k, "v":v})
        for m in data["member"]:
            m["ref"] = str(m["ref"])
            self.Element("member", m)
        self.endElement("relation")
      
def NodeToXml(data, full = False):
    o = io.StringIO()
    w = OsmSaxWriter(o, "UTF-8")
    if full:
        w.startDocument()
        w.startElement("osm", {})
    if data:
        w.NodeCreate(data)
    if full:
        w.endElement("osm")
    return o.getvalue()

def WayToXml(data, full = False):
    o = io.StringIO()
    w = OsmSaxWriter(o, "UTF-8")
    if full:
        w.startDocument()
        w.startElement("osm", {})
    if data:
        w.WayCreate(data)
    if full:
        w.endElement("osm")
    return o.getvalue()

def RelationToXml(data, full = False):
    o = io.StringIO()
    w = OsmSaxWriter(o, "UTF-8")
    if full:
        w.startDocument()
        w.startElement("osm", {})
    if data:
        w.RelationCreate(data)
    if full:
        w.endElement("osm")
    return o.getvalue()
