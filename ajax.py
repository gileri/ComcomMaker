#!/usr/bin/env python3
import urllib.parse
import sys, os
abspath = os.path.dirname(__file__)
sys.path.append(abspath)
os.chdir(abspath)

import settings


class Status(object):

    def __init__(self, code, msg):
        self.code=code
        self.msg=msg

    @property
    def http_format(self):
        return str(self.code) + " " + self.msg


def get_relation(rid):
    # downloads relation from osm.org and stores in cache
    from modules import HTTPRequest
    req = HTTPRequest.httpRequest(
        server=settings.api_server,
        api= settings.api_path,
        created_by= settings.created_by
    )
    rel = req.get(settings.api_path+'relation/'+str(abs(rid))+'/full')
    return rel


def get_entity(params):

    import psycopg2, psycopg2.extras
    from psycopg2 import sql
    rows = [{'osm_id':-1,
            'name':'debuging SQL',
            'wkt':'POINT (%(lon)s, %(lat)s)' % {'lat': params['lat'],'lon': params['lon']}
            }] # dummy result for debugging
    conn = psycopg2.connect("dbname='%(dbname)s' user='%(user)s' host='%(host)s' password='%(password)s'"% {
                                'dbname':settings.db_name,
                                'user':settings.db_user,
                                'host':settings.db_host,
                                'password':settings.db_password
                            })
    curs = conn.cursor(cursor_factory=psycopg2.extras.DictCursor)
    assert(len(params['where']))
    where = sql.SQL(" AND ").join([
        sql.SQL("{} = {}").format(sql.Identifier(w[0]), sql.Literal(w[1]))
        for w in params["where"]
    ])
    q = sql.SQL("""
SELECT osm_id AS osm_id,
    name AS name,
    ST_AsText(ST_Collect(ST_Transform({geometry}, 4326))) AS wkt
FROM {table}
WHERE
    ST_WITHIN(ST_Transform(ST_SetSRID(ST_POINT({lon}, {lat}), 4326), {srid}), {geometry})
    AND {where}
GROUP BY osm_id, name;
    """).format(
        table=sql.Identifier(settings.db_table),
        geometry=sql.Identifier(settings.db_geometry),
        srid=sql.Literal(int(settings.db_srid)),
        lat=sql.Literal(params['lat']),
        lon=sql.Literal(params['lon']),
        where=where,
    )
    if not settings.db_debug:
        curs.execute(q)  # comment this line for sql debugging
        rows = curs.fetchall() # comment this line for sql debugging
    return rows


def main(form):

    from modules.ccm import save, normalize, tidy
    tidy(settings.cache_dir, settings.cache_deadline)
    if "lat" not in form or "lon" not in form:
        return Status(400, "Lat and lon were not given."), []
    params = {
        'where': settings.db_where,
        'lat': form['lat'][0],
        'lon': form['lon'][0],
        }
    if "where" in form:
        params['where'] = [
            t.split('=')[0:2]
            for t in form['where'][0].split('\n')
        ]
    rows = get_entity(params)
    if len(rows) == 0:
        status = Status(204, "Nothing found.")
        data = "{}"
    if len(rows) == 1:
        status = Status(200, "OK.")
        data = '{"id":"%(osm_id)s","name":"%(name)s","wkt":"%(wkt)s"}' % rows[0]
#        rid = rows[0]['osm_id']
#        osm=get_relation(rid)
#        save(osm,settings.cache_dir+normalize(rid)+'.osm')
        
    if len(rows) > 1:
        status = Status(206, "Several result, first sent.")
        data = '{"id":"%(osm_id)s","name":"%(name)s"}' % rows[0]
#        rid = str(abs(rows[0]['osm_id']))
#        osm=get_relation(rid)
    return status, data


def application(environ, start_response):
    status, data = main(dict(urllib.parse.parse_qs(environ['QUERY_STRING'])))
    start_response(status.http_format, [("Content-Type", "application/json;charset:utf-8")])
    nested = '{"status":[%i,"%s"],"data":%s}' % (status.code, status.msg, data)
    return [nested.encode('utf-8')]
